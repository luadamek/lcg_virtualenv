#!/usr/bin/env bash
# Enable bash strict mode
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -eu
IFS=$'\n\t'

source lcg_virtualenv_helpers.sh

if [ -n "${BASH:-}" ]; then
    assert_equal() {
        test_i=$((test_i+1))
        if [ -z "${!1+x}" ]; then
            n_failed=$((n_failed+1))
            echo "   FAILED ${test_i}: ${1} is unset and not equal to ${2}"
        elif [ "${!1}" = "${2}" ]; then
            echo "   PASSED ${test_i}"
        else
            n_failed=$((n_failed+1))
            echo "   FAILED ${test_i}: ${1} (${!1}) is not equal to ${2}"
        fi
    }

    assert_not_set() {
        test_i=$((test_i+1))
        if [ -z "${!1+x}" ] ; then
            echo "   PASSED ${test_i}"
        else
            n_failed=$((n_failed+1))
            echo "   FAILED ${test_i}: ${1} is set to ${!1}"
        fi
    }
elif [ -n "${ZSH_VERSION:-}" ]; then
    assert_equal() {
        test_i=$((test_i+1))
        if [ -z "${(P)1+x}" ]; then
            n_failed=$((n_failed+1))
            echo "   FAILED ${test_i}: ${1} is unset and not equal to ${2}"
        elif [ "${(P)1}" = "${2}" ]; then
            echo "   PASSED ${test_i}"
        else
            n_failed=$((n_failed+1))
            echo "   FAILED ${test_i}: ${1} (${(P)1}) is not equal to ${2}"
        fi
    }

    assert_not_set() {
        test_i=$((test_i+1))
        if [ -z "${(P)1+x}" ] ; then
            echo "   PASSED ${test_i}"
        else
            n_failed=$((n_failed+1))
            echo "   FAILED ${test_i}: ${1} is set to ${(P)1}"
        fi
    }
else
    echo "ERROR: Your shell is not supported"
fi

test_i=0
n_failed=0

assert_not_set EXAMPLE_VAR
_lcg_virtualenv_set_variable EXAMPLE_VAR "TEST TEST"
assert_equal EXAMPLE_VAR "TEST TEST"
_lcg_virtualenv_unset_variable EXAMPLE_VAR
assert_not_set EXAMPLE_VAR

export EXAMPLE_VAR="SOME OLD VALUE"
_lcg_virtualenv_set_variable EXAMPLE_VAR "TEST TEST"
assert_equal EXAMPLE_VAR "TEST TEST"
_lcg_virtualenv_unset_variable EXAMPLE_VAR
assert_equal EXAMPLE_VAR "SOME OLD VALUE"
unset EXAMPLE_VAR

assert_not_set EXAMPLE_VAR
_lcg_virtualenv_modify_variable EXAMPLE_VAR "A PREFIX:" ":A SUFFIX"
assert_equal EXAMPLE_VAR "A PREFIX::A SUFFIX"
_lcg_virtualenv_unset_variable EXAMPLE_VAR
assert_not_set EXAMPLE_VAR

export EXAMPLE_VAR="SOME OLD VALUE"
_lcg_virtualenv_modify_variable EXAMPLE_VAR "A PREFIX:" ":A SUFFIX"
assert_equal EXAMPLE_VAR "A PREFIX:SOME OLD VALUE:A SUFFIX"
_lcg_virtualenv_unset_variable EXAMPLE_VAR
assert_equal EXAMPLE_VAR "SOME OLD VALUE"
unset EXAMPLE_VAR

if [ $n_failed -gt 0 ]; then
    echo "ERROR $n_failed tests failed"
    exit 1
fi
