#!/usr/bin/env python
import argparse
import json
import os
from os.path import dirname, join
import shutil
import sys


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('virtual_env_dir')
    args = parser.parse_args()

    write_activation_scripts(args.virtual_env_dir)


def write_activation_scripts(virtual_env_dir):
    old_env = json.loads(os.environ['OLD_ENV'])
    new_env = json.loads(os.environ['NEW_ENV'])

    replaced_vars = {}
    edited_vars = {}

    for key in old_env:
        if key not in new_env:
            raise NotImplementedError(key+' is unset by the LCG view, this '
                                      'doesn\'t happen?')

    for key, value in new_env.items():
        # Ignore some keys
        if key in ['_', 'PWD', 'OLDPWD']:
            continue
        # Key was added by the LCG view
        elif key not in old_env:
            replaced_vars[key] = value
        # The key hasn't changed
        elif old_env[key] == value:
            continue
        # The old value was empty
        elif old_env[key] == '':
            replaced_vars[key] = value
        # The key was replaced by the LCG view
        elif old_env[key] not in value:
            replaced_vars[key] = value
        # Something has gone wrong...
        elif value.count(old_env[key]) != 1:
            print((key, value, old_env[key]))
            raise ValueError(('Found multiple entries of the original value in '
                              '{key}, has the LCG view already been sourced?')
                             .format(**locals()))
        # The variable has been edited from the original variable
        else:
            assert value.count(old_env[key]) == 1, (key, value, old_env[key])
            split_var = value.split(old_env[key])
            if len(split_var) == 2:
                prefix, suffix = split_var
            elif len(split_var) == 1:
                if value.index(old_env[key]) == 0:
                    prefix = None
                    suffix = split_var[0]
                else:
                    if value.index(old_env[key]) != len(value)-len(old_env[key]):
                        raise NotImplementedError(
                            'Changes are always inserted in the beggining or end?',
                            key, value, old_env[key]
                        )
                    prefix = split_var[0]
                    suffix = None
            else:
                raise ValueError(key, value, old_env[key])
            edited_vars[key] = [prefix, suffix]

    activate_lines = []
    deactivate_lines = []
    # Set variables used by the LCG view
    for key, value in replaced_vars.items():
        activate_lines.append('_lcg_virtualenv_set_variable "{key}" "{value}"'.format(**locals()))
        deactivate_lines.append('_lcg_virtualenv_unset_variable "{key}"'.format(**locals()))
    # Modify variables unused by the LCG view
    for key, (prefix, suffix) in edited_vars.items():
        activate_lines.append('_lcg_virtualenv_modify_variable "{key}" "{prefix}" "{suffix}"'.format(**locals()))
        deactivate_lines.append('_lcg_virtualenv_unset_variable "{key}"'.format(**locals()))

    # Manually set PYTHONPATH so import order is correct
    user_site_dir = virtual_env_dir+'/lib/python{major}.{minor}/site-packages'
    user_site_dir = user_site_dir.format(major=sys.version_info.major, minor=sys.version_info.minor)
    activate_lines.append('export PYTHONPATH="'+user_site_dir+':$PYTHONPATH"')

    hepers_script_fn = join(virtual_env_dir, 'bin', '.lcg_virtualenv_helpers.sh')
    shutil.copy(join(dirname(__file__), 'lcg_virtualenv_helpers.sh'), hepers_script_fn)

    # Read the activation script
    activate_fn = join(virtual_env_dir, 'bin', 'activate')
    with open(activate_fn, 'rt') as fp:
        activate_script = fp.read()

    # Edit the activation section
    new_activate_script = 'source {hepers_script_fn}\n'.format(**locals())
    new_activate_script += '\n'.join(activate_lines) + '\n'

    # Edit the deactivate section
    deactivate_end_start = activate_script.index('deactivate () {')
    deactivate_end_end = activate_script.index('\n    unset VIRTUAL_ENV\n', deactivate_end_start)
    new_activate_script += activate_script[:deactivate_end_end]
    new_activate_script += '\n    if [ ! "${1-}" = "nondestructive" ] ; then'
    deactivate_lines = [' '*8+l for l in deactivate_lines]
    new_activate_script += '\n' + '\n'.join(deactivate_lines)
    new_activate_script += '\n        unset -f _lcg_virtualenv_set_variable'
    new_activate_script += '\n        unset -f _lcg_virtualenv_unset_variable'
    new_activate_script += '\n        unset -f _lcg_virtualenv_modify_variable'
    new_activate_script += '\n    fi'
    new_activate_script += activate_script[deactivate_end_end:]

    # Write modified script
    with open(activate_fn, 'wt') as fp:
        fp.write(new_activate_script)


if __name__ == '__main__':
    parse_args()
