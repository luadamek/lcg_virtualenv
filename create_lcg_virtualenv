#!/usr/bin/env bash
# Enable bash strict mode
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

VIEWS_DIRECTORY=/cvmfs/sft.cern.ch/lcg/views
LCG_VERSION=LCG_94python3

function print_help() {
    echo "Valid usage is:"
    echo " ./create_lcg_virtualenv PATH_TO_VIRTUALENV"
    echo " ./create_lcg_virtualenv PATH_TO_VIRTUALENV LCG_VERSION"
    exit "$1"
}


if [ "$#" -eq 0 ] || [ "$#" -gt 2 ]; then
    echo "ERROR: Invalid number of arguments passed"
    print_help 1
elif [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
    print_help 0
elif [ "$#" -eq 1 ]; then
    PATH_TO_VIRTUALENV=$1
elif [ "$#" -eq 2 ]; then
    PATH_TO_VIRTUALENV=$1
    LCG_VERSION=$2
else
    echo "This should be unreachable..."
    exit 100
fi

if [[ $PATH_TO_VIRTUALENV == -* ]]; then
    echo "ERROR: The path to the virtualenv must not start with a \"-\""
    exit 8
fi

PATH_TO_VIRTUALENV="$(python -c "import os; print(os.path.abspath('${PATH_TO_VIRTUALENV}'))")"

if [ ! -f "${VIEWS_DIRECTORY}/setupViews.sh" ]; then
    echo "ERROR: /cvmfs/sft.cern.ch does not seem to be available"
    exit 2
fi

if [ ! -d "${VIEWS_DIRECTORY}/${LCG_VERSION}" ]; then
    echo "ERROR: ${LCG_VERSION} does not seem to be a valid LCG version"
    exit 3
fi

if [ -z ${CMTCONFIG+x} ]; then
    echo "ERROR: CMTCONFIG is not set"
    exit 4
fi

if [ ! -d "${VIEWS_DIRECTORY}/${LCG_VERSION}/${CMTCONFIG}" ]; then
    echo "ERROR: CMTCONFIG ${CMTCONFIG} does not appear to be valid for $LCG_VERSION"
    exit 5
fi

if [ -d "${PATH_TO_VIRTUALENV}" ]; then
    echo "ERROR: ${PATH_TO_VIRTUALENV} already exists"
    exit 6
fi

# Track the changes made by setting up the LCG view
OLD_ENV="$(python -c 'import os, json; print(json.dumps(dict(os.environ)))')"
# https://sft.its.cern.ch/jira/browse/SPI-1136
unset R_HOME
set +u
source "${VIEWS_DIRECTORY}/setupViews.sh" "${LCG_VERSION}" "${CMTCONFIG}"
set -u
if [ -n "${BASH:-}" ] || [ -n "${ZSH_VERSION:-}" ] ; then
    hash -r
fi
NEW_ENV="$(python -c 'import os, json; print(json.dumps(dict(os.environ)))')"

# LCG93python3 doesn't define a python binary so we have to use python3 instead
PYTHON_BINARY=python
if [ "$LCG_VERSION" == "LCG_93python3" ]; then
    PYTHON_BINARY=python3
fi

# Install virtualenv for the current user
$PYTHON_BINARY -m pip install --user --upgrade virtualenv==16.7.9

# Create the virtual environment
$PYTHON_BINARY -m virtualenv "${PATH_TO_VIRTUALENV}"

# Patch the virtual environment so it works with LCG
export OLD_ENV
export NEW_ENV
CURRENT_FILE_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
$PYTHON_BINARY "${CURRENT_FILE_PATH}/compare_environments.py" "${PATH_TO_VIRTUALENV}"


# Check everything went smoothly
if [ ! -d "${PATH_TO_VIRTUALENV}" ] ||
   [ ! -f "${PATH_TO_VIRTUALENV}/bin/activate" ] ||
   [ ! -f "${PATH_TO_VIRTUALENV}/bin/python" ]; then
    echo "ERROR: Unknown error creating ${PATH_TO_VIRTUALENV}"
    exit 7
fi

# Print some helpful information
echo "Virtual environment successfully created"
echo "It can be activated using:"
echo "   source ${PATH_TO_VIRTUALENV}/bin/activate"
echo "It can then be deactivated using"
echo "   deactivate"
