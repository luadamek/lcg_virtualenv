if [ -n "${BASH:-}" ]; then
    _lcg_virtualenv_set_variable() {
        var_name=$1
        new_value=$2
        if ! [ -z "${!var_name+x}" ] ; then
            old_value_name="_OLD_LCG_VIRTUAL_$1"
            eval "$old_value_name=\"${!var_name}\""
        fi
        export "${var_name}=${new_value}"
    }

    _lcg_virtualenv_unset_variable() {
        var_name=$1
        old_value_name="_OLD_LCG_VIRTUAL_$1"
        if ! [ -z "${!old_value_name+x}" ] ; then
            export "${var_name}=${!old_value_name}"
            unset "${old_value_name}"
        else
            unset "${var_name}"
        fi
    }

    _lcg_virtualenv_modify_variable() {
        var_name=$1
        new_prefix=$2
        new_suffix=$3
        if [ -z "${!var_name+x}" ] ; then
            eval "$var_name=\"\""
        else
            old_value_name="_OLD_LCG_VIRTUAL_$1"
            eval "$old_value_name=\"${!var_name}\""
        fi
        export "${var_name}=${new_prefix}${!var_name}${new_suffix}"
    }
elif [ -n "${ZSH_VERSION:-}" ]; then
    _lcg_virtualenv_set_variable() {
        var_name=$1
        new_value=$2
        if ! [ -z "${(P)var_name+x}" ] ; then
            old_value_name="_OLD_LCG_VIRTUAL_$1"
            eval "$old_value_name=\"${(P)var_name}\""
        fi
        export "${var_name}=${new_value}"
    }

    _lcg_virtualenv_unset_variable() {
        var_name=$1
        old_value_name="_OLD_LCG_VIRTUAL_$1"
        if ! [ -z "${(P)old_value_name+x}" ] ; then
            export "${var_name}=${(P)old_value_name}"
            unset "${old_value_name}"
        else
            unset "${var_name}"
        fi
    }

    _lcg_virtualenv_modify_variable() {
        var_name=$1
        new_prefix=$2
        new_suffix=$3
        if [ -z "${(P)var_name+x}" ] ; then
            eval "$var_name=\"\""
        else
            old_value_name="_OLD_LCG_VIRTUAL_$1"
            eval "$old_value_name=\"${(P)var_name}\""
        fi
        export "${var_name}=${new_prefix}${(P)var_name}${new_suffix}"
    }
else
    echo "ERROR: Your shell is not supported"
fi
